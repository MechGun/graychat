//
// Test quiz "Creating simple chat"
//

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/websocket"
)

//
// struct for message
//
type Message struct {
	Type    string // "authConfirm","authReject","message","userList":send to slave, "authRequest","message":send to master
	From    string // username
	To      string // username
	SessKey string // ???? future feature =)
	Avatar  string // filename for avatar icon
	Msg     string // message text
	Time    string // server time
}

//
// handler websocket connections
//
func wsHandler(res http.ResponseWriter, req *http.Request) {
	var upgr = websocket.Upgrader{}
	conn, err := upgr.Upgrade(res, req, nil)
	if err != nil {
		http.NotFound(res, req)
		log.Println("WS err:", err)
		return
	}

	log.Println("WS open ==> local:", conn.LocalAddr(), " remote:", conn.RemoteAddr())
	m := Message{
		Type:    "authReject",
		From:    "Ken",
		To:      "Barby",
		SessKey: "big$ecretwowwow",
		Avatar:  "123.png",
		Msg:     "Stop. No way.",
		Time:    "18:00",
	}
	err = conn.WriteJSON(m)
	if err != nil {
		fmt.Println("SendJSON2WS error:", err)
	} else {
		log.Println("JSON sended.")
	}
	message := ""
	for {
		err := conn.ReadJSON(message)
		if err != nil {
			break
		}
		log.Println("Msg:", message)
	}
	log.Println("WS closed.")
}

//
// handler 4 homepage, send chat frontend 4 user
//
func homeHandler(res http.ResponseWriter, req *http.Request) {
	http.ServeFile(res, req, "pub/index.html")
	log.Println("HTTP server is up.")
}

func main() {
	//	Json testing
	var f Message
	m := Message{
		Type:    "authReject",
		From:    "Ken",
		To:      "Barby",
		SessKey: "big$ecretwowwow",
		Avatar:  "123.png",
		Msg:     "Stop. No way.",
		Time:    "18:00",
	}
	fmt.Println(m)
	b, err := json.Marshal(m)
	if err != nil {
		fmt.Println("error:", err)
	}
	os.Stdout.Write(b)
	json.Unmarshal(b, &f)
	fmt.Println(f)

	//	Start HTTP server
	serverAddr := "127.0.0.1:8080"
	http.HandleFunc("/ws", wsHandler)
	http.HandleFunc("/", homeHandler)
	http.ListenAndServe(serverAddr, nil)

}
